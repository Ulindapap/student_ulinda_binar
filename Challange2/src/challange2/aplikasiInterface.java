/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package challange2;

import java.util.ArrayList;

/**
 *
 * @author ulind
 */
public interface aplikasiInterface {
    ArrayList<Integer> read();
    
    int modus(ArrayList<Integer>modus);
    void median(ArrayList<Integer>median);
    double mean(ArrayList<Integer>rerata);
    void write(String txtFile);
}
