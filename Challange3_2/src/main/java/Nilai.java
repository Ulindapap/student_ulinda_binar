package Challange3_2;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Nilai {
    private ArrayList<Integer>nilai;

    Nilai(ArrayList<Integer>nilai) {
        this.nilai = nilai;
    }
    public ArrayList<Integer> getNilai() {
        return nilai;
    }

    public void setNilai(ArrayList<Integer> nilai) {
        this.nilai = nilai;
    }

    public double getmean() {
        double mean = nilai.stream().collect(Collectors.averagingDouble(s -> s ));
        return mean;

    }
    public float getmedian() {
        float median = 0;
        if(nilai.size()%2 == 0) {
            median = (nilai.get(nilai.size()/2) +  nilai.get(nilai.size() /2 -1)) /2 ;
        } else {
            median = nilai.get(nilai.size() / 2);
        }
        return median;
    }
    public int getfrekuensi(int min, int max) {
        int count = 0;
        for(int i = 0; i < nilai.size(); i ++) {
            if (nilai.get(i) >= min && nilai.get(i) <=max ) {
                count ++;
            }
        }
        return count;

    }
    public int getmodus() {
        int modus = 0;
        int jumlah_terbanyak = 0;
        for(int n : nilai) {
            int frekuensi = getfrekuensi(n,n);
            if (frekuensi > jumlah_terbanyak) {
                modus = n;
                jumlah_terbanyak = frekuensi;
            }
        }
        return modus;
    }
}
