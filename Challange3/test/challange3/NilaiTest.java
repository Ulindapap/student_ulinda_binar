/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package challange3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ulind
 */
public class NilaiTest {
    
    public NilaiTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNilai method, of class Nilai.
     */
    //@Test
   /* public void testGetNilai() {
        System.out.println("getNilai");
        Nilai instance = null;
        ArrayList<Integer> expResult = null;
        ArrayList<Integer> result = instance.getNilai();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }*/

    /**
     * Test of setNilai method, of class Nilai.
     */
   /* @Test
    public void testSetNilai() {
        System.out.println("setNilai");
        ArrayList<Integer> nilai = null;
        Nilai instance = null;
        instance.setNilai(nilai);
        // TODO review the generated test code and remove the default call to fail.
    }*/

    /**
     * Test of getmean method, of class Nilai.
     */
    @Test
    public void testGetmean() {
        System.out.println("getmean");
        ArrayList<Integer> mean = new ArrayList<>(Arrays.asList(2,4,3,5,6,7,1));
        Nilai instance = new Nilai(mean);
        double expResult = 4.0;
        double result = instance.getmean();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getmedian method, of class Nilai.
     */
    @Test
    public void testGetmedian() {
        System.out.println("getmedian");
        ArrayList<Integer> median = new ArrayList<>(Arrays.asList(2,4,3,5,6,7,1));
        Collections.sort(median);
        Nilai instance = new Nilai(median);
        float expResult = 4;
        float result = instance.getmedian();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getfrekuensi method, of class Nilai.
     */
    @Test
    public void testGetfrekuensi() {
        System.out.println("getfrekuensi");
        ArrayList<Integer> frekuensi = new ArrayList<>(Arrays.asList(2,4,3,5,6,7,1,2));
        Nilai instance = new Nilai(frekuensi);
        int expResult = 2;
        int result = instance.getfrekuensi(2, 2);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getmodus method, of class Nilai.
     */
    @Test
    public void testGetmodus() {
        System.out.println("getmodus");
        ArrayList<Integer> modus = new ArrayList<>(Arrays.asList(2,4,3,5,6,7,1,2));
        Nilai instance = new Nilai(modus);
        int expResult = 2;
        int result = instance.getmodus();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
