/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package challange3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 *
 * @author ulind
 */
public class Challange3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)throws IOException {
        // TODO code application logic here
        ArrayList<Integer>dataNilai = read("C:\\binar\\student_ulinda_binar\\Challange3\\src\\challange3\\data_sekolah.csv");
		Collections.sort(dataNilai);
		Nilai nilai = new Nilai(dataNilai);
		pilihan(nilai);
		
	}
	public static ArrayList<Integer> read(String data_sekolah) {
		ArrayList<Integer> nilaiSiswa = new ArrayList<>();
		
		try {
			File file = new File(data_sekolah);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			String[] tempArr;
			while((line = br.readLine()) != null) {
				tempArr = line.split(";");
				for (int i  = 1; i < tempArr.length - 1; i++) {
					nilaiSiswa.add(Integer.valueOf(tempArr[i]));
				}
			}
			br.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
			System.out.println("------------------------------------------------------------------------------------------");
			System.out.println("Aplikasi Pengolah Nilai Siswa");
			System.out.println("------------------------------------------------------------------------------------------");
			System.out.println("File Tidak di temukan");
			System.out.println("Letakkan file csv dengan nama file data_sekolah di direktori");
			System.out.println("0. Exit");
			System.out.println("------------------------------------------------------------------------------------------");
			
			Scanner input = new Scanner(System.in);
			int masukkan = input.nextInt();
			
			if (masukkan ==0) {
				System.exit(0);
			}
			
		}
		return nilaiSiswa;
	}
	public static void keseluruhan(Nilai nilai) throws IOException {
		String path = "C:\\binar\\student_ulinda_binar\\Challange3\\src\\challange3\\seluruh.txt";
		File file = new File(path);
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		
		writer.write("Berikut hasil Pengolahan Nilai; ");
		writer.newLine();
		writer.write("Rata-Rata : " + nilai.getmean());
		writer.newLine();
		writer.write("Median : " + nilai.getmedian());
		writer.newLine();
		writer.write("Modus : " + nilai.getmodus());
		writer.newLine();
		
		writer.close();
		
		System.out.println(" File telah di generate di " + path);
	}
	public static void frekuensi (Nilai nilai) throws IOException {
		String path = "C:\\binar\\student_ulinda_binar\\Challange3\\src\\challange3\\frekuensi.txt";
		File file = new File(path);
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		writer.write("Berikut hasil Pengolahan Nilai:       ");
		writer.newLine();
		writer.write(" ");
		writer.newLine();
		writer.write("      Nilai       	|        Frekuensi ");
		writer.newLine();
		writer.write("Kurang dari 6           |	 " + nilai.getfrekuensi(0, 5));
		writer.newLine();
		writer.write("            6      	|      " + nilai.getfrekuensi(6, 6));
		writer.newLine();
		writer.write("            7      	|      " + nilai.getfrekuensi(7, 7));
		writer.newLine();
		writer.write("            8      	|      " + nilai.getfrekuensi(8, 8));
		writer.newLine();
		writer.write("            9      	|      " + nilai.getfrekuensi(9, 9));
		writer.newLine();
		writer.write("           10      	|      " + nilai.getfrekuensi(10, 10));
		writer.newLine();
		
		writer.close();
	}
	public static void pilihan(Nilai nilai) throws IOException {
		//boolean ulang = true;
		while (true) {
		String pilihapa;
		System.out.println("------------------------------------------------------------------------------------------");
		System.out.println("Aplikasi Pengolah Nilai Siswa");
		System.out.println("------------------------------------------------------------------------------------------");
		System.out.println("Letakkan file csv dengan nama file data_sekolah di direktori");
		System.out.println("berikut : D:\\binar\\student_binar\\Challenge2\\src\\Challenge2");
		System.out.println(" ");
		System.out.println("pilih menu :");
		System.out.println("1. Generate txt untuk menampilkan mean, median, modus");
		System.out.println("2. Generate txt untuk menampilkan frekuensi");
		System.out.println("3. Generate kedua file");
		System.out.println("0. Exit");
		System.out.println("------------------------------------------------------------------------------------------");
		
		Scanner input = new Scanner(System.in);
		String pilih = input.nextLine();
		switch(pilih) {
		case "1" :
			keseluruhan(nilai);
			System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
			System.out.println("Aplikasi Pengolah Nilai Siswa");
			System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
			System.out.println("Letakkan file csv dengan nama file data_sekolah di direktori");
			System.out.println("File telah di generate di C:\\binar\\student_ulinda_binar\\Challange3\\src\\challange3\\seluruh.txt");
			System.out.println("silahkan cek");
			System.out.println(" ");
			System.out.println("1. Kembali ke menu utama");
			System.out.println("0. Exit");
			System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
		    	pilihapa = input.nextLine();
		    		switch(pilihapa) {
		    		case "1" : 
		    			continue;
		    		case "0" :
		    		System.exit(0);
		    		default:
	 					System.out.println("Input yang Anda masukkan salah");
	 					
		    	}
		    		
		 case "2" :
			frekuensi(nilai);
			System.out.println("----------------------------------------------------------------------------------------------------------------------------");
			System.out.println("Aplikasi Pengolah Nilai Siswa");
			System.out.println("----------------------------------------------------------------------------------------------------------------------------");
			System.out.println("Letakkan file csv dengan nama file data_sekolah di direktori");
			System.out.println("C:\\binar\\student_ulinda_binar\\Challange3\\src\\challange3\\frekuensi.txt");
			System.out.println("silahkan cek");
			System.out.println(" ");
			System.out.println("1. Kembali ke menu utama");
			System.out.println("0. Exit");
			System.out.println("----------------------------------------------------------------------------------------------------------------------------");
		    	pilihapa = input.nextLine();
		    		switch(pilihapa) {
		    		case "1" : 
		    			continue;
		    		case "0" :
		    		System.exit(0);
		    		}
		 case "3" :
			 keseluruhan(nilai);
			 frekuensi(nilai);
			 System.out.println("");
				System.out.println("----------------------------------------------------------------------------------------------------------------------------");
				System.out.println("Aplikasi Pengolah Nilai Siswa");
				System.out.println("----------------------------------------------------------------------------------------------------------------------------");
				System.out.println("Letakkan file csv dengan nama file data_sekolah di direktori");
				System.out.println("File telah di generate di C:\\binar\\student_ulinda_binar\\Challange3\\src\\challange3\\seluruh.txt");
				System.out.println("File telah di generate di C:\\binar\\student_ulinda_binar\\Challange3\\src\\challange3\\frekuensi.txt");
				System.out.println("silahkan cek");
				System.out.println(" ");
				System.out.println("1. Kembali ke menu utama");
				System.out.println("0. Exit");
				System.out.println("----------------------------------------------------------------------------------------------------------------------------");
			    	pilihapa = input.nextLine();
			    		switch(pilihapa) {
			    		case "1" : 
			    			continue;
			    		case "0" :
			    		System.exit(0);
			    		}
		 case "0" :
			 System.exit(0);
		}
		}
		}
    }
    

