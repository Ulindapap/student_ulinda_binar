package com.room4.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.room4.model.Authors;

public interface AuthorsRepository extends JpaRepository<Authors, Long>{

}
