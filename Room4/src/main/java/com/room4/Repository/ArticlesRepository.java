package com.room4.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.room4.model.Articles;

public interface ArticlesRepository extends JpaRepository<Articles, Long>  {

}
