package com.room4.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.room4.model.Categories;

public interface CategoriesRepository extends JpaRepository<Categories, Long>{

}
