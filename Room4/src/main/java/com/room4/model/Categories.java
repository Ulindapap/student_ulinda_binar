package com.room4.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Categories {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	 	private Long id;
    	private String category_name;
    	
    	@OneToMany(targetEntity = Articles.class, cascade = CascadeType.ALL)
    	@JoinColumn(name = "categories_id", referencedColumnName = "id")
    	private List<Articles> article;
    	

}
