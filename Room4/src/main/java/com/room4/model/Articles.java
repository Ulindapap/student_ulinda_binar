package com.room4.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Articles {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	 	private Long id;
    	private Long authors_id;
    	private Long categories_id;
    	private String title;
    	
    	@Column(columnDefinition = "TEXT")
    	private String content;
    	
	
	

}
