package com.room4.Service;

import java.util.List;

import com.room4.model.Categories;

public interface CategoriesService {

	Categories addCategory(Categories category);
    Categories getCategory(Long id);
    List<Categories> getAllCategory();
    Categories updateCategory(Categories category);
    Categories deleteCategory(Long id);
}
