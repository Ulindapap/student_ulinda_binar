package com.room4.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.room4.Repository.AuthorsRepository;
import com.room4.model.Authors;

@Service

public class AuthorsServiceImpl implements AuthorsService {
	
	@Autowired
	AuthorsRepository authorsRepository;

	@Override
	public Authors addAuthor(Authors article) {
		// TODO Auto-generated method stub
		return authorsRepository.save(article);
	}

	@Override
	public Authors getAuthor(Long id) {
		// TODO Auto-generated method stub
		return authorsRepository.findById(id).orElseThrow();
	}

	@Override
	public List<Authors> getAllAuthor() {
		// TODO Auto-generated method stub
		return authorsRepository.findAll();
	}

	@Override
	public Authors updateAuthor(Authors author) {
		// TODO Auto-generated method stub
		Authors res = authorsRepository.findById(author.getId()).orElse(new Authors());
        if(res.getId() != null) {
            res.setName(author.getName());
            return authorsRepository.save(res);
        } else {
            return res;
        }
	}

	@Override
	public Authors deleteAuthor(Long id) {
		// TODO Auto-generated method stub
		Authors res = authorsRepository.findById(id).orElseThrow();
        if(res.getId() != null) {
            authorsRepository.deleteById(id);
            return res;
        } else {
            return new Authors();
        }
	}

	
	

}
