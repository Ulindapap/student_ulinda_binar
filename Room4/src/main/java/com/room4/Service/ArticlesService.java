package com.room4.Service;

import java.util.List;

import com.room4.model.Articles;

public interface ArticlesService {
	Articles addArticles(Articles article);
	Articles getArticle(Long id);
    List<Articles> getAllArticle();
    Articles updateArticle(Articles article);
    Articles deleteArticle(Long id);
	
}
