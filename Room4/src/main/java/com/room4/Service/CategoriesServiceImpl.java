package com.room4.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.room4.Repository.CategoriesRepository;
import com.room4.model.Categories;

@Service

public class CategoriesServiceImpl implements CategoriesService {
	
	@Autowired
	CategoriesRepository categoriesRepository;
	

	@Override
	public Categories addCategory(Categories article) {
		// TODO Auto-generated method stub
		return categoriesRepository.save(article);
	}

	@Override
	public Categories getCategory(Long id) {
		// TODO Auto-generated method stub
		return categoriesRepository.findById(id).orElseThrow();
	}

	@Override
	public List<Categories> getAllCategory() {
		// TODO Auto-generated method stub
		return categoriesRepository.findAll();
	}

	@Override
	public Categories updateCategory(Categories category) {
		// TODO Auto-generated method stub
		if(categoriesRepository.findById(category.getId()).isPresent()) {
            return categoriesRepository.save(category);
        } else {
            return new Categories();
        }
			}

	@Override
	public Categories deleteCategory(Long id) {
		// TODO Auto-generated method stub
		Categories res = categoriesRepository.findById(id).orElseThrow();
        if(res.getId() != null) {
            categoriesRepository.deleteById(id);
            return res;
        } else {
            return new Categories();
        }
	}

}
