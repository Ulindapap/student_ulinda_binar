package com.room4.Service;

import java.util.List;

import com.room4.model.Authors;

public interface AuthorsService {
	Authors addAuthor(Authors author);
    Authors getAuthor(Long id);
    List<Authors> getAllAuthor();
    Authors updateAuthor(Authors author);
    Authors deleteAuthor(Long id);

}
