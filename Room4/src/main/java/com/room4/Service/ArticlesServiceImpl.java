package com.room4.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.room4.Repository.ArticlesRepository;
import com.room4.model.Articles;

@Service

public class ArticlesServiceImpl implements ArticlesService {
	
	@Autowired
	ArticlesRepository articlesRepository;
	

	@Override
	public Articles addArticles(Articles article) {
		// TODO Auto-generated method stub
		return articlesRepository.save(article);
	}

	@Override
	public Articles getArticle(Long id) {
		// TODO Auto-generated method stub
		return articlesRepository.findById(id).orElseThrow();
	}

	@Override
	public List<Articles> getAllArticle() {
		// TODO Auto-generated method stub
		return articlesRepository.findAll();
	}

	@Override
	public Articles updateArticle(Articles article) {
		// TODO Auto-generated method stub
		if(articlesRepository.findById(article.getId()).isPresent()) {
            return articlesRepository.save(article);
        } else {
            return new Articles();
        }
	}

	@Override
	public Articles deleteArticle(Long id) {
		// TODO Auto-generated method stub
		Articles res = articlesRepository.findById(id).orElseThrow();
        if(res.getId() != null) {
            articlesRepository.deleteById(id);
            return res;
        } else {
            return new Articles();
        }
	}
	
	

}
