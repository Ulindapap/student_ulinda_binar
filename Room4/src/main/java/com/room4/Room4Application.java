package com.room4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Room4Application {

	public static void main(String[] args) {
		SpringApplication.run(Room4Application.class, args);
	}

}
