package com.room4.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.room4.Service.ArticlesService;
import com.room4.model.Articles;

@RestController
@RequestMapping("/article")

public class ArticlesController {
	@Autowired
    ArticlesService articlesService;

    
    @PostMapping
    public Articles addArticle(@RequestBody Articles article) {
        return articlesService.addArticles(article);
    }
    



   
    @GetMapping
    public List<Articles> getAllArticles() {
        return articlesService.getAllArticle();
    }

    @GetMapping("/{article_id}")
    public Articles getArticle(@PathVariable Long article_id) {
        return articlesService.getArticle(article_id);
    }
    



    
    @PutMapping("/updatearticle")
    public ResponseEntity<String> updateArticle(@RequestBody Articles article) {
        Articles res = articlesService.updateArticle(article);

        if(res.getId() == null) {
            return new ResponseEntity<String>("No Article Found.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<String>("Article Updated.", HttpStatus.OK);
        }
    }
    



    
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteArticle(@PathVariable Long id) {
        Articles res = articlesService.deleteArticle(id);

        if(res.getId() == null) {
            return new ResponseEntity<String>("No Article Found.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<String>("Article Deleted.", HttpStatus.OK);
        }
    }

}
