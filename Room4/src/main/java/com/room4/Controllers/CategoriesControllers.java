package com.room4.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.room4.Service.CategoriesService;
import com.room4.model.Categories;

@RestController
@RequestMapping("/category")

public class CategoriesControllers {
	@Autowired
    CategoriesService categoriesService;

    
    @PostMapping
    public Categories addCategory(@RequestBody Categories category) {
        System.out.println(category.getCategory_name());
        return categoriesService.addCategory(category);
    }
   

    
    @GetMapping
    public List<Categories> getAllCategories() {
        return categoriesService.getAllCategory();
    }

    @GetMapping("/{category_id}")
    public Categories getCategory(@PathVariable Long category_id) {
        return categoriesService.getCategory(category_id);
    }
   

    @PutMapping("/updatecategory")
    public ResponseEntity<String> updateCategory(@RequestBody Categories category) {
        Categories res = categoriesService.updateCategory(category);

        if(res.getId() == null) {
            return new ResponseEntity<String>("No Category Found.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<String>("Category Updated.", HttpStatus.OK);
        }
    }
    



    
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCategory(@PathVariable Long id) {
        Categories res = categoriesService.deleteCategory(id);

        if(res.getId() == null) {
            return new ResponseEntity<String>("No Category Found.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<String>("Category Deleted.", HttpStatus.OK);
        }
    }
}
