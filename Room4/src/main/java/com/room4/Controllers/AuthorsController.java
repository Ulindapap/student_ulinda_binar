package com.room4.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.room4.Service.AuthorsService;
import com.room4.model.Authors;

@RestController
@RequestMapping("/author")

public class AuthorsController {
	@Autowired
    AuthorsService authorsService;

    
    @PostMapping
    public Authors addAuthor(@RequestBody Authors author) {
        return authorsService.addAuthor(author);
    }
    



    
    @GetMapping
    public List<Authors> getAllAuthors() {
        return authorsService.getAllAuthor();
    }

    @GetMapping("/{author_id}")
    public Authors getAuthor(@PathVariable Long author_id) {
        return authorsService.getAuthor(author_id);
    }
    



    
    @PutMapping("/updateauthor")
    public ResponseEntity<String> updateAuthor(@RequestBody Authors author) {
        Authors res = authorsService.updateAuthor(author);

        if(res.getId() == null) {
            return new ResponseEntity<String>("No Author Found.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<String>("Author Updated.", HttpStatus.OK);
        }
    }
    



   
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteAuthor(@PathVariable Long id) {
        Authors res = authorsService.deleteAuthor(id);

        if(res.getId() == null) {
            return new ResponseEntity<String>("No Author Found.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<String>("Author Deleted.", HttpStatus.OK);
        }
    }

}
