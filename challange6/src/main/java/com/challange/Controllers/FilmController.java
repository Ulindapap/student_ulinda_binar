package com.challange.Controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.challange.Entity.FilmEntity;
import com.challange.Entity.ScheduleEntity;
import com.challange.Services.FilmService;



@RestController
@RequestMapping("/film")
public class FilmController {
	@Autowired
	 private FilmService filmService;
	
    @GetMapping("/")
    public Iterable<FilmEntity> getAllFilms() {
        return filmService.findAllFilm();
    }
    @GetMapping("/tayang")
    public Iterable<FilmEntity> getFilmsIsShowing() {
        return filmService.findAllFilm(true);
    }
    @GetMapping("/{id}")
    public Optional<FilmEntity> getFilmById(@PathVariable("id") int id) {
        return filmService.findFilmById(id);
    }
    @GetMapping("/{id}/schedule")
    public Iterable<ScheduleEntity> getScheduleByFilmId(@PathVariable("id") int id) {
        return filmService.findScheduleByFilmId(id);
    }
    @PostMapping("/")
    public FilmEntity addFilm(@RequestBody FilmEntity film) {
        return filmService.saveFilm(film);
    }
    @PutMapping("/{id}/schedule")
    public FilmEntity addSchedule(
            @PathVariable("id") int id,
            @RequestBody ScheduleEntity schedule
    ) {
        return filmService.addSchedule(id, schedule);
    }
    @PutMapping("/{code}/schedule/{scheduleId}")
    public FilmEntity removeSchedule(
            @PathVariable("code") int id,
            @PathVariable("scheduleId") int scheduleId
    ) {
        return filmService.deleteSchedule(id, scheduleId);
    }
    
    @PutMapping("/{code}")
    public FilmEntity updateTitle(@PathVariable int code, @RequestParam String judul) {
        return filmService.updateFilmTitle(code, judul);
    }

    @DeleteMapping("/{id}")
    public String deleteFilm(@PathVariable("id") int id) {
        return filmService.deleteFilmById(id);
    }




	

}
