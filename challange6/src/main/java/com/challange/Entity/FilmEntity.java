package com.challange.Entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor


public class FilmEntity {
	 @Id @GeneratedValue(strategy = GenerationType.AUTO)
	 @Column(length = 10 ,unique = true, nullable = false)
	 private Integer filmcode;
	 private String judul;
	 private boolean tayang;
	 
	 @OneToMany(targetEntity = ScheduleEntity.class, cascade = CascadeType.ALL)
	    @JoinColumn(name = "film_code", referencedColumnName = "code")
	    private List<ScheduleEntity> schedules;

	 
	 

}
