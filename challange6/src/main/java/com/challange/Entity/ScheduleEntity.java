package com.challange.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor

public class ScheduleEntity {
	
	 @Id @GeneratedValue(strategy = GenerationType.AUTO)
	 
	@Column(length = 10 ,unique = true, nullable = false)
	private Integer scheduleId;
	 @Column(name = "film_code")
	 private Integer filmCode;
	private  String tanggal;
	private Long harga;
	private String start_time;
	private String end_time;
	
	

}
