package com.challange.Services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challange.Entity.FilmEntity;
import com.challange.Entity.ScheduleEntity;
import com.challange.Repository.FilmRepository;

@Service
public class FilmService {
	@Autowired
	
    private FilmRepository filmRepository; 
	
	public FilmEntity saveFilm(FilmEntity film) {
        return filmRepository.save(film);
    }
	public Iterable<FilmEntity> findAllFilm() {
        return filmRepository.findAll();
    }
	 public Iterable<FilmEntity> findAllFilm(boolean tayang) {
	        return filmRepository.findAllBytayang(tayang);
	    }
	 public Optional<FilmEntity> findFilmById(int id) {
	        return filmRepository.findById(id);
	    }
	 public String deleteFilmById(int id) {
	        FilmEntity film = filmRepository.findById(id).isPresent()
	                ? filmRepository.findById(id).get()
	                : null;
	        if (film != null) {
	            filmRepository.delete(film);
	            return "Film dengan id " + id + " berhasil dihapus";
	        }
	        return "Film dengan id " + id + " tidak ditemukan";
	    }
	 public FilmEntity addSchedule(int id, ScheduleEntity schedule) {
	        FilmEntity film = filmRepository.findById(id).isPresent()
	                ? filmRepository.findById(id).get()
	                : null;
	        if (film != null) {
	            schedule.setFilmCode(film.getfilmcode());
	            film.getSchedules().add(schedule);
	            return filmRepository.save(film);
	        }
	        return null;
	    }
	 public FilmEntity deleteSchedule(int filmId, int scheduleId) {
	        FilmEntity film = filmRepository.findById(filmId).isPresent()
	                ? filmRepository.findById(filmId).get()
	                : null;
	        if (film != null) {
	            film.getTanggal().removeIf(schedule -> schedule.getId() == scheduleId);
	            return filmRepository.save(film);
	        }
	        return null;
	    }
	 public FilmEntity updateFilmTitle(int id, String judul) {
	        FilmEntity film = findFilmById(id).isPresent()
	                ? findFilmById(id).get()
	                : null;
	        if (film != null) {
	            film.setJudul(judul);
	            return filmRepository.save(film);
	        }
	        return null;
	    }
	 public Iterable<ScheduleEntity> findScheduleByFilmId(int id) {
	        FilmEntity film = filmRepository.findById(id).isPresent()
	                ? filmRepository.findById(id).get()
	                : null;
	        if (film != null) {
	            return film.getSchedules();
	        }
	        return null;
	    }




}
