package com.challange.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.challange.Entity.FilmEntity;

public interface FilmRepository extends JpaRepository<FilmEntity, Integer> {
	Iterable<FilmEntity> findAllBytayang(boolean tayang);
}
