package com.challange.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.challange.Entity.UsersEntity;



public interface UsersRepository extends JpaRepository<UsersEntity, Integer> {
	UsersEntity findByUsername(String username);
}
