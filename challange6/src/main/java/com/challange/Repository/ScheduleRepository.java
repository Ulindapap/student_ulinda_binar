package com.challange.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.challange.Entity.ScheduleEntity;

public interface ScheduleRepository extends JpaRepository<ScheduleEntity, Integer>{
	

}
