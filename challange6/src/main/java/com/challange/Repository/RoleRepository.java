package com.challange.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.challange.Entity.RoleEntity;



public interface RoleRepository extends JpaRepository<RoleEntity, Integer>  {
	RoleEntity findByName(String name);
}
