package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.users;

public interface usersRepository extends JpaRepository<users, Integer> {

}
