package com.example.demo.usersServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;

import com.example.demo.entity.users;
import com.example.demo.repository.usersRepository;
import com.example.demo.service.usersService;

public class usersServiceImpl implements usersService {

	  @Autowired
	  usersRepository repository;

	  @Override
	  public users addusers(users user) {
	    // TODO Auto-generated method stub
	    return repository.save(user);
	  }

	  @Override
	  public users getusersbyId(int userId) {
	    // TODO Auto-generated method stub
	    return repository.findById(userId).get();
	  }

	  @Override
	  public void updateUsers(users user) {
	    // TODO Auto-generated method stub
	      repository.save(user);
	    
	  }

	  @Override
	  public void deleteUsers(int user) {
	    // TODO Auto-generated method stub
	    try {
	      repository.deleteById(user);
	    } catch (DataAccessException ex) {
	      throw new RuntimeException(ex.getMessage());
	    }
	  }

	  @Override
	  public List<users> getAllusers() {
	    // TODO Auto-generated method stub
	    return repository.findAll();
	  }

}
