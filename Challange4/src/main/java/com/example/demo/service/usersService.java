package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.users;

public interface usersService {
	users addusers(users username) ;
	users getusersbyId(int userId);
	
	void updateUsers(users user);
	void deleteUsers(int user);
	
	List<users> getAllusers();
	
}
