package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.users;
import com.example.demo.service.usersService;

@RestController
@RequestMapping("/users")
public class usersController {

	  @Autowired
	  usersService usersservice;
	  
	  @PostMapping
	  @ResponseStatus(HttpStatus.CREATED)
	  public users adduser(@RequestBody users user) {
	    return usersservice.addusers(user);
	  }
	  
	  @GetMapping
	  public List<users> getAllusers(){
	    return usersservice.getAllusers();
	  }
	  
	  @GetMapping("/{Id}")
	  public users getuserbyId(@PathVariable("Id") int userId) {
	    return usersservice.getusersbyId(userId);
	  }
	  
	  @PutMapping("updateUsers/")
	  public ResponseEntity<String> updateUsers(@RequestBody users user){
	    try {
	      usersservice.updateUsers(user);
	      return new ResponseEntity<String>(HttpStatus.OK);
	    } catch (Exception e) {
	      System.out.println(e.getMessage());
	      return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	    }
	  }
	  
	  @DeleteMapping("/{Id}")
	  public ResponseEntity<String> deleteUser (@PathVariable int id){
	    try {
	      usersservice.deleteUsers(id);
	      return new ResponseEntity<String>(HttpStatus.OK);
	    } catch (Exception e) {
	      // TODO: handle exception
	      System.out.println(e.getMessage());
	      return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	    }
	    
	  }

}
