/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Challange1;

import java.util.Scanner;

/**
 *
 * @author ulind
 */
public class Kalkulator {
    
    public static void Persegi(){
        Scanner input = new Scanner(System.in);
        double s, luas,keliling;
        System.out.println("\nPersegi");
        System.out.print("Masukkan Panjang sisi : ");
        s = input.nextDouble();
        System.out.println(" ");
        luas = s * s;
        keliling = 4* s;
        System.out.print("Luas  = " + (int)luas + " \nKeliling = "+ (int)keliling);
        System.out.println(" ");
    }
    public static void Segitiga(){
        Scanner input = new Scanner(System.in);
        double a , t, luas,keliling;
        System.out.println("Segitiga");
        System.out.print("Masukkan Panjang alas : ");
        a = input.nextDouble();
        System.out.print("Masukkan tinggi : ");
        t = input.nextDouble();
        System.out.println(" ");
        luas = 0.5 * a * t;
        keliling = 3 * t;
        System.out.print("Luas  = " + (int)luas + " \nKeliling = "+ (int)keliling);
        System.out.println("");
    }
    public static void PersegiPanjang(){
        Scanner input = new Scanner(System.in);
        double p , l, luas,keliling;
        System.out.println("\nPersegi Panjang");
        System.out.print("Masukkan Panjang : ");
        p = input.nextDouble();
        System.out.print("Masukkan lebar : ");
        l = input.nextDouble();
        System.out.println(" ");
        luas = p * l;
        keliling = 2*p + 2*l;
        System.out.print("Luas  = " + (int)luas + " \nKeliling  = "+ (int)keliling);
        System.out.println("");
    }
    public static void Lingkaran(){
        Scanner input = new Scanner(System.in);
        double r, luas, keliling;
        System.out.println("\nLingkaran");
        System.out.println("Masukkan Jari-jari lingkaran : ");
        r = input.nextDouble();
        System.out.println(" ");
        luas = 3.14 * r * r;
        keliling = 3.24 * 2 * r;
        System.out.println("Luas Lingkaran = " + (int)luas);
        System.out.println(" ");
    }
    public static void Kubus(){
        Scanner input = new Scanner(System.in);
        double s, volume;
        System.out.println("Anda Memilih Kubus");
        System.out.println("Masukkan Sisi kubus : ");
        s = input.nextDouble();
        System.out.println(" ");
        volume = s * s * s;
        System.out.println("Volume Kubus = " + (int)volume);
        System.out.println(" ");
    }
    public static void Balok(){
        Scanner input = new Scanner(System.in);
        double p,l,t, volume;
        System.out.println("Anda Memilih Balok");
        System.out.println("Masukkan Panjang : ");
        p = input.nextDouble();
        System.out.println("Masukkan Lebar : ");
        l = input.nextDouble();
        System.out.println("Masukkan Tinggi: ");
        t = input.nextDouble();
        System.out.println(" ");
        volume = p*l*t;
        System.out.println("Volume Balok = " + (int)volume);
        System.out.println(" ");
    }
    public static void Tabung(){
        Scanner input = new  Scanner(System.in);
        double r, t, volume;
        System.out.println("Anda Memilih Tabung");
        System.out.println("Masukkan jari - jari : ");
        r = input.nextDouble();
        System.out.println("Masukkan tinggi");
        t = input.nextDouble();
        System.out.println(" ");
        volume = 3.14 * r * r * t;
        System.out.println("Volume Tabung = " + (int)volume);
        System.out.println(" ");
        
    }
}
   
