package com.challange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Challange45678Application {

	public static void main(String[] args) {
		SpringApplication.run(Challange45678Application.class, args);
	}

}
