package com.challange.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.challange.Entity.Schedule;

public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

}
