package com.challange.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.challange.Entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);

}
