package com.challange.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.challange.Entity.Film;

public interface FilmRepository extends JpaRepository<Film, Long> {
	Iterable<Film> findAllByIsShowing(boolean isShowing);

}
