package com.challange.Controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.challange.Dto.ScheduleDto;
import com.challange.Entity.Schedule;
import com.challange.Service.ScheduleService;



@RestController
@RequestMapping("/schedule")


public class ScheduleController {

	@Autowired
    private ScheduleService scheduleService;

    @GetMapping("/")
    public Iterable<Schedule> getAllSchedule() {
        return scheduleService.getAllSchedule();
    }

    @GetMapping("/{id}")
    public Optional<ScheduleDto> getScheduleById(@PathVariable("id") Long id) {
        return scheduleService.getScheduleById(id);
    }

    @PostMapping("/")
    public Schedule addSchedule(@RequestBody Schedule schedule) {
        return scheduleService.saveSchedule(schedule);
    }

    @DeleteMapping("/{id}")
    public String deleteSchedule(@PathVariable("id") Long id) {
        return scheduleService.deleteSchedule(id);
    }

}
