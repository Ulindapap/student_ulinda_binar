package com.challange.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challange.Dto.UserDto;
import com.challange.Entity.User;
import com.challange.Repository.UserRepository;



@Service

public class UserService {
	@Autowired
    private UserRepository userRepository;

    public User save(User user) {
        return userRepository.save(user);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public Iterable<User> findAllUsers() {
        return userRepository.findAll();
    }

    public User findUserById(Long id) {
        return userRepository.findById(id).isPresent()
                ? userRepository.findById(id).get()
                : null;
    }

    public String deleteUserById(Long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            return "User tidak ditemukan";
        }
        userRepository.deleteById(id);
        return "User dengan id " + id + " telah dihapus";
    }

    public User updateUser(long id, UserDto userDto) {
        User user = findUserById(id);
        if (user == null) {
            throw new IllegalArgumentException("User not found");
        }
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
        return userRepository.save(user);
    }


	

}
