package com.challange.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challange.Entity.Film;
import com.challange.Entity.Schedule;
import com.challange.Repository.FilmRepository;



@Service

public class FilmService {
	@Autowired
    private FilmRepository filmRepository;

    public Film saveFilm(Film film) {
        return filmRepository.save(film);
    }

    public Iterable<Film> findAllFilm() {
        return filmRepository.findAll();
    }

    public Iterable<Film> findAllFilm(boolean isShowing) {
        return filmRepository.findAllByIsShowing(isShowing);
    }

    public Optional<Film> findFilmById(long id) {
        return filmRepository.findById(id);
    }

    public String deleteFilmById(long id) {
        Film film = filmRepository.findById(id).isPresent()
                ? filmRepository.findById(id).get()
                : null;
        if (film != null) {
            filmRepository.delete(film);
            return "Film dengan id " + id + " berhasil dihapus";
        }
        return "Film dengan id " + id + " tidak ditemukan";
    }

    public Film addSchedule(long id, Schedule schedule) {
        Film film = filmRepository.findById(id).isPresent()
                ? filmRepository.findById(id).get()
                : null;
        if (film != null) {
            schedule.setFilmCode(film.getCode());
            film.getSchedules().add(schedule);
            return filmRepository.save(film);
        }
        return null;
    }

    public Film deleteSchedule(long filmId, long scheduleId) {
        Film film = filmRepository.findById(filmId).isPresent()
                ? filmRepository.findById(filmId).get()
                : null;
        if (film != null) {
            film.getSchedules().removeIf(schedule -> schedule.getId() == scheduleId);
            return filmRepository.save(film);
        }
        return null;
    }

    public Film updateFilmTitle(long id, String title) {
        Film film = findFilmById(id).isPresent()
                ? findFilmById(id).get()
                : null;
        if (film != null) {
            film.setTitle(title);
            return filmRepository.save(film);
        }
        return null;
    }

    public Iterable<Schedule> findScheduleByFilmId(long id) {
        Film film = filmRepository.findById(id).isPresent()
                ? filmRepository.findById(id).get()
                : null;
        if (film != null) {
            return film.getSchedules();
        }
        return null;
    }

}
