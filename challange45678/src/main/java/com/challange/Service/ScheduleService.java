package com.challange.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challange.Dto.ScheduleDto;
import com.challange.Entity.Film;
import com.challange.Entity.Schedule;
import com.challange.Repository.ScheduleRepository;



@Service

public class ScheduleService {
	@Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private FilmService filmService;

    public Iterable<Schedule> getAllSchedule() {
        return scheduleRepository.findAll();
    }

    public Optional<ScheduleDto> getScheduleById(Long id) {
        Optional<Schedule> reqSchedule = scheduleRepository.findById(id);
        if (reqSchedule.isEmpty()) {
            throw new IllegalArgumentException("Jadwal tidak ditemukan");
        }
        Schedule schedule = reqSchedule.orElse(null);
        Film film = filmService.findFilmById(schedule.getFilmCode()).isPresent()
                ? filmService.findFilmById(schedule.getFilmCode()).get()
                : null;
        if (film == null) {
            throw new IllegalArgumentException("Film tidak ditemukan");
        }
        String filmTitle = film.getTitle();
        ScheduleDto scheduleDto = new ScheduleDto(
        );
        return Optional.of(scheduleDto);
    }

    public Schedule saveSchedule(Schedule schedule) {
        Optional<Film> film = filmService.findFilmById(schedule.getFilmCode());
        if (film.isEmpty()) {
            throw new IllegalArgumentException("Film not found");
        }
        return scheduleRepository.save(schedule);
    }

    public String deleteSchedule(Long id) {
        Optional<Schedule> schedule = scheduleRepository.findById(id);
        if (schedule.isEmpty()) {
            return "Jadwal tidak ditemukan";
        }
        scheduleRepository.deleteById(id);
        return "Jadwal dengan ID " + id + " berhasil dihapus";
    }


}
