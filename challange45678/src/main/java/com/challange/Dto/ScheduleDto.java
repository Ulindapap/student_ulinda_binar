package com.challange.Dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor


public class ScheduleDto implements  Serializable{
	private Long id;
    private String date;
    private Long price;
    private String startTime;
    private String endTime;
    private ScheduleFilm film;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ScheduleFilm {
        private Long code;
        private String title;
    }


}
