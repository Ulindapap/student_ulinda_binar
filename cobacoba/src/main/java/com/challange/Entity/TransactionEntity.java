package com.challange.Entity;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor

public class TransactionEntity {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer TransactionId;
    private String TransactionStatus;
    private Date TransactionDate;
}
