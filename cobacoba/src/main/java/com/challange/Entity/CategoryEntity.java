package com.challange.Entity;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor

public class CategoryEntity {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer CategoryId;
    private String CategoryName;
    
    @OneToMany(targetEntity = ProductEntity.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "CategoryId", referencedColumnName = "CategoryId")
    private Collection<ProductEntity> catgoryId = new ArrayList<>();
}
