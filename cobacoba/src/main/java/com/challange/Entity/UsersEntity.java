package com.challange.Entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor

public class UsersEntity {
	 @Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	     private Integer UsersId;
	    private String UsersName;
	    @Column(length = 255, unique = true, nullable = false)
	    private String Username;
	    private String Password;
	    private String NoTelp;

	    @ManyToMany(fetch = FetchType.LAZY)
	    private Collection<RoleEntity> roles = new ArrayList<>();

	   @OneToMany(targetEntity = ProductEntity.class, cascade = CascadeType.ALL)
	    @JoinColumn(name = "UsersId", referencedColumnName = "UsersId")
	    private List<ProductEntity> UserId;
	   
	   @OneToMany(targetEntity = WishlistEntity.class, cascade = CascadeType.ALL)
	    @JoinColumn(name = "UsersId", referencedColumnName = "UsersId")
	    private Collection<WishlistEntity> UserIdW = new ArrayList<>();
	   
	   @OneToMany(targetEntity = TransactionEntity.class, cascade = CascadeType.ALL)
	    @JoinColumn(name = "UsersId", referencedColumnName = "UsersId")
	    private Collection<TransactionEntity> UserIdT = new ArrayList<>();


}
