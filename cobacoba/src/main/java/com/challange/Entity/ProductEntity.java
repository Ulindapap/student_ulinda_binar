package com.challange.Entity;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor

public class ProductEntity {
	 @Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    public Integer ProductId;
	    public String ProductName;
	    public Integer productJumlah;
	    public Integer ProductHarga;
	    public Blob ProductPicture;
	    public String productLocation;
	    public String ProductDescription;
	    
	    @OneToMany(targetEntity = WishlistEntity.class, cascade = CascadeType.ALL)
	    @JoinColumn(name = "ProductId", referencedColumnName = "productId")
	    private Collection<WishlistEntity> ProductIdW = new ArrayList<>();
	    
	    @OneToMany(targetEntity = TransactionEntity.class, cascade = CascadeType.ALL)
	    @JoinColumn(name = "ProductId", referencedColumnName = "ProductId")
	    private Collection<TransactionEntity> ProductIdT = new ArrayList<>();
}
