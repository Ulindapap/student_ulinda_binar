package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Challange4yyApplication {

	public static void main(String[] args) {
		SpringApplication.run(Challange4yyApplication.class, args);
	}

}
