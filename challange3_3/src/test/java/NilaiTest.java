import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class NilaiTest {
    @Test
    void testGetmean(){
        System.out.println("getmean");
        ArrayList<Integer> mean = new ArrayList<>(Arrays.asList(2,4,3,5,6,7,1));
        Nilai instance = new Nilai(mean);
        double expResult = 4.0;
        double result = instance.getmean();
        assertEquals(expResult, result, 0.0);
    }
    @Test
    void testGetmedian() {
        System.out.println("getmedian");
        ArrayList<Integer> median = new ArrayList<>(Arrays.asList(2, 4, 3, 5, 6, 7, 1));
        Collections.sort(median);
        Nilai instance = new Nilai(median);
        float expResult = 4;
        float result = instance.getmedian();
        assertEquals(expResult, result, 0.0);
    }
    @Test
    void testGetfrekuensi() {
        System.out.println("getfrekuensi");
        ArrayList<Integer> frekuensi = new ArrayList<>(Arrays.asList(2,4,3,5,6,7,1,2));
        Nilai instance = new Nilai(frekuensi);
        int expResult = 2;
        int result = instance.getfrekuensi(2, 2);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    @Test
    void testGetmodus() {
        System.out.println("getmodus");
        ArrayList<Integer> modus = new ArrayList<>(Arrays.asList(2,4,3,5,6,7,1,2));
        Nilai instance = new Nilai(modus);
        int expResult = 2;
        int result = instance.getmodus();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }



}
