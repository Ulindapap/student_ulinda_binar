package com.challange.Entity;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
public class UsersEntity {
	 @Id @GeneratedValue(strategy = GenerationType.AUTO)
	    private Integer id;
	    private String name;
	    @Column(length = 255, unique = true,nullable = false)
	    private String username;
	    private String password;
	    @ManyToMany(fetch = FetchType.LAZY)
	    private Collection<RoleEntity> roles = new ArrayList<>();

}
