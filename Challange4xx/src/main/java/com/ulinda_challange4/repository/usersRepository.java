package com.ulinda_challange4.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ulinda_challange4.entity.users;

public interface usersRepository extends JpaRepository<users, Integer> {

}
