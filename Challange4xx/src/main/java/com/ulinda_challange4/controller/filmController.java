package com.ulinda_challange4.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ulinda_challange4.entity.film;
import com.ulinda_challange4.service.filmService;

@RestController
@RequestMapping("/film")
public class filmController {
	
	@Autowired
	filmService filmservice;
	
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public film addfilm(@RequestBody film name) {
		return filmservice.addfilm(name);
	}
	
	@GetMapping
	@ResponseStatus(HttpStatus.CREATED)
	public List<film> getAllfilm(){
		return filmservice.getAllfilm();
	}
	
	
	@GetMapping("/{Id}")
	public film getfilm(@PathVariable("Id") int filmCode) {
		return filmservice.getfilm(filmCode);
	}
	
	@PutMapping
	public ResponseEntity<String> updatefilm(@RequestBody film name){
		try {
			filmservice.updatefilm(name);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping("/{Id}")
	public ResponseEntity<String> deletefilm(@RequestBody film name){
		try {
			filmservice.deletefilm(name);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
	}
	
	
	

}
