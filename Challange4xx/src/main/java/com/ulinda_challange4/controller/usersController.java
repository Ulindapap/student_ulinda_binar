package com.ulinda_challange4.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ulinda_challange4.entity.users;
import com.ulinda_challange4.service.usersService;

@RestController
@RequestMapping("/users")

public class usersController {
	@Autowired
	usersService usersservice;
	
	@PostMapping 
	@ResponseStatus(HttpStatus.CREATED)
	public users addusers(@RequestBody users username) {
		return usersservice.addusers(username);
		
	}
	
	@GetMapping
	@ResponseStatus(HttpStatus.CREATED)
	public List<users> getAllusers(){
		return usersservice.getAllusers();
	}
	
	@GetMapping("/{Id}")
	public users getusersbyId(@PathVariable("Id") int userId) {
		return usersservice.getusersbyId(userId);
	}
	
	@PutMapping("updateusers/")
	public  ResponseEntity<String> updateusers(@RequestBody users username){
		try {
			usersservice.updateusers(username);
			return new ResponseEntity<String>(HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
			
		}
		}
	@DeleteMapping("/{Id}")
	public ResponseEntity<String> deleteusers(@RequestBody users username){
		try {
			usersservice.deleteusers(username);
			return new ResponseEntity<String>(HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
	}

}
