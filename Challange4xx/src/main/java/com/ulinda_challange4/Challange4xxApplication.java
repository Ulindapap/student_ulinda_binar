package com.ulinda_challange4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Challange4xxApplication {

	public static void main(String[] args) {
		SpringApplication.run(Challange4xxApplication.class, args);
	}

}
