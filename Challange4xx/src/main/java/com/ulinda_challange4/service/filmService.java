package com.ulinda_challange4.service;

import java.util.List;



import com.ulinda_challange4.entity.film;


public interface filmService {
	film addfilm(film name);
	film getfilm(int filmCode);
	
	void updatefilm(film name);
	void deletefilm(film name);
	
	List<film> getAllfilm();
	

}
