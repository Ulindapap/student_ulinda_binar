package com.ulinda_challange4.service;

import java.util.List;

import com.ulinda_challange4.entity.users;



public interface usersService {
	users addusers(users username);
	users getusersbyId(int userId);
	
	void updateusers(users username);
	void deleteusers(users username);
	
	List<users> getAllusers();
	
	
	
}
