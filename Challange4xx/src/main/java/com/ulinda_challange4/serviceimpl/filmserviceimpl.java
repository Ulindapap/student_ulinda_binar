package com.ulinda_challange4.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.ulinda_challange4.entity.film;
import com.ulinda_challange4.repository.filmRepository;
import com.ulinda_challange4.service.filmService;

@Service

public class filmserviceimpl implements filmService {
	
	@Autowired
	filmRepository repository;
	

	@Override
	public film addfilm(film name) {
		// TODO Auto-generated method stub
		return repository.save(name);
	}

	@Override
	public film getfilm(int filmCode) {
		// TODO Auto-generated method stub
		return repository.findById(filmCode).get();
	}

	@Override
	public void updatefilm(film name) {
		// TODO Auto-generated method stub
		repository.save(name);
		
	}

	@Override
	public void deletefilm(film name) {
		// TODO Auto-generated method stub
		try {
			repository.delete(name);
			
		} catch (DataAccessException ex) {
			// TODO: handle exception
			throw new RuntimeException(ex.getMessage());
		}
		
	}

	@Override
	public List<film> getAllfilm() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}
	

}
