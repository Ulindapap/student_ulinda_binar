package com.ulinda_challange4.serviceimpl;

import java.util.List;

import javax.management.RuntimeErrorException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.ulinda_challange4.entity.users;
import com.ulinda_challange4.repository.usersRepository;
import com.ulinda_challange4.service.usersService;
@Service
public class usersserviceimpl implements usersService {
	
	@Autowired
	usersRepository repository;
	
	@Override
	public users addusers(users username) {
		// TODO Auto-generated method stub
		return repository.save(username);
		
	}@Override
	public users getusersbyId(int userId) {
		// TODO Auto-generated method stub
		return repository.findById(userId).get();
		
	}@Override
	public void deleteusers(users username) {
		// TODO Auto-generated method stub
		try {
			repository.delete(username);
			
		} catch (DataAccessException ex) {
			// TODO: handle exception
			throw new RuntimeException(ex.getMessage());
			
		}
		
	}@Override
	public List<users> getAllusers() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}@Override
	public void updateusers(users username) {
		// TODO Auto-generated method stub
		repository.save(username);
		
	}

}
