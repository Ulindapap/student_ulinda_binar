package com.ulinda_challange4.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter

public class film {
	@Id
	private int filmCode;
	private String name;
	private String tayang;
	@OneToMany(targetEntity = schedule.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "filmCode", referencedColumnName = "filmCode")
	private List<schedule> jadwal;
	

}
