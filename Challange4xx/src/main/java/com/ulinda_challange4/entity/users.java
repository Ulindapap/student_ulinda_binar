package com.ulinda_challange4.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter

public class users {
	@Id
	private int userId;
	private String username;
	private String email;
	private String password;

}
