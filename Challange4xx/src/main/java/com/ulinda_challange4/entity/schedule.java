package com.ulinda_challange4.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter

public class schedule {
	@Id
	@GeneratedValue
	private int scheduleId;
	private LocalDate tanggalTayang;
	private LocalDateTime mulai;
	private LocalDateTime selesai;
	private int harga;

}
